<?php

namespace Drupal\pepper_crt\Service;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LogLevel;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ContentRebuildTrigger
{

  /**
   * @var HttpClientInterface
   */
  protected $client;

  protected $logger;

  public function __construct(ClientFactory $client_factory, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->client = $client_factory->fromOptions([
      'verify' => FALSE,
    ]);
    $this->logger = $loggerChannelFactory->get('pepper_crt');
  }

  public function triggerRebuild() {
    // Check if the service url is configured.
    $serviceUrl = Settings::get('content_rebuild_trigger.service_url');
    $this->logger->log(LogLevel::INFO, 'Trigger content rebuild service at: ' . $serviceUrl);
    if ($serviceUrl) {
      try {
        $this->logger->log(LogLevel::INFO, 'Trigger content rebuild now (' . $serviceUrl . ')');
        $this->client->post($serviceUrl, []);
      } catch (GuzzleException $e) {

      }
    }
  }
}
